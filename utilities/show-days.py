import sys
import csv
import numpy
from matplotlib import pyplot

days = []

with open(f'results/{sys.argv[1]}.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0

    for row in csv_reader:
        if line_count > 0:
            days.append(int(row[-1]))

        line_count += 1

pyplot.hist(days)
pyplot.show()
