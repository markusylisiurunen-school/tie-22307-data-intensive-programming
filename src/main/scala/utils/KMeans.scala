package utils

import scala.math.{floor, sin, cos, atan2, Pi}
import org.apache.spark.rdd.RDD

import utils._

object KMeans {

  // TRANSFORMERS (column -> feature group)

  /**
    * Transform a column value to a double.
    */
  def transformToDouble(col: String): Array[Double] = {
    Array(col.toDouble)
  }

  /**
    * Transform a colum value to the index of the day in a week.
    */
  def transformToDayOfWeekIndex(col: String): Array[Double] = {
    val (x, y) = TaskUtils.dayOfWeekToFeatures(col)
    Array(x, y)
  }

  // OUTPUTS (feature group -> output column(s))

  /**
    * Output a double.
    */
  def outputDouble(featureGroup: Array[Double]): Array[String] = {
    Array(featureGroup(0).toString)
  }

  /**
    * Output the index of the day based on x and y values.
    */
  def outputDayOfWeekIndex(featureGroup: Array[Double]): Array[String] = {
    val dayOfWeekIndex =
      TaskUtils.dayOfWeekFeaturesToIndex(featureGroup(0), featureGroup(1))
    Array(dayOfWeekIndex.toString)
  }

  // K-MEANS HELPERS

  /**
    * Check if two mean groups are the same (end condition for K-means).
    */
  def areSame(a: Array[Array[Double]], b: Array[Array[Double]]): Boolean = {
    val combined = a.zip(b).flatMap({ case (x, y) => x.zip(y) })
    val error = combined.map({ case (x, y) => (x - y).abs }).reduce(_ + _)

    error <= Double.MinPositiveValue
  }

  /**
    * Cluster each feature group to the nearest mean.
    */
  def clusters(means: Array[Array[Double]],
               features: RDD[Array[Double]]): RDD[(Int, Array[Double])] =
    features.map(feature => {
      val distances =
        means.map(mean => TaskUtils.euclideanDistance(mean, feature))
      val nearest = distances.zipWithIndex.minBy(_._1)

      (nearest._2, feature)
    })

  /**
    * Calculate the new mean for each cluster of features.
    */
  def means(clusters: RDD[(Int, Array[Double])]): RDD[Array[Double]] =
    clusters
      .mapValues(feature => (feature, 1))
      .reduceByKey({
        case ((f1, c1), (f2, c2)) =>
          (f1.zip(f2).map({ case (x, y) => x + y }), c1 + c2)
      })
      .map({
        case (_, (features, count)) =>
          features.map(feature => feature / count.toDouble)
      })

  /**
    * Run the KMeans algorithm for a collection of feature vectors.
    */
  def apply(features: RDD[Array[Double]], k: Int): Array[Array[Double]] = {
    var result = features.takeSample(false, k)
    var i = 1

    while (i != -1) {
      TaskUtils.log(s"KMeans: Running iteration $i...")

      val featureClusters = clusters(result, features)
      val featureMeans = means(featureClusters)
        .collect()
        .sortWith((a, b) => {
          val origin = Array.fill(a.length) { 0.0 }

          val aDistance = TaskUtils.euclideanDistance(a, origin)
          val bDistance = TaskUtils.euclideanDistance(b, origin)

          aDistance < bDistance
        })

      val done = areSame(featureMeans, result)

      result = featureMeans

      i = if (done) -1 else i + 1
    }

    result
  }
}
