package utils

import scala.math._

object TaskUtils {

  /**
    * Print log messages in testing environment.
    */
  def log(message: String): Unit = {
    if (sys.env.getOrElse("DIP_ENV", "") == "testing")
      println(message)
  }

  /**
    * Calculate the Euclidean distance.
    */
  def euclideanDistance(a: Array[Double], b: Array[Double]): Double = {
    sqrt(a.zip(b).map({ case (x, y) => pow(x - y, 2) }).sum)
  }

  /**
    * Convert the day of week to features.
    */
  def dayOfWeekToFeatures(dayOfWeek: String): (Double, Double) = {
    val dayOfWeekIndex = dayOfWeek.toLowerCase match {
      case "maanantai"   => 0
      case "tiistai"     => 1
      case "keskiviikko" => 2
      case "torstai"     => 3
      case "perjantai"   => 4
      case "lauantai"    => 5
      case "sunnuntai"   => 6
    }

    val x = cos(2 * Pi * dayOfWeekIndex / 7.0)
    val y = sin(2 * Pi * dayOfWeekIndex / 7.0)

    (x, y)
  }

  /**
    * Convert day of week features to index.
    */
  def dayOfWeekFeaturesToIndex(x: Double, y: Double): Int = {
    var theta = atan2(y, x)
    theta = if (theta >= 0) theta else theta + 2 * Pi
    floor(theta / (2 * Pi) * 7).toInt + 1
  }
}
