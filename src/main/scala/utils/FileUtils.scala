package utils

import java.io.{File, FileWriter, BufferedWriter}
import org.apache.spark.ml.linalg.Vector

object FileUtils {

  /**
    * Writes a header and content to a csv file. If the file does not exist, it is created.
    */
  def write(fileName: String,
            header: Array[String],
            data: Array[Array[String]]): Unit = {
    val file = new File(fileName)

    if (!file.exists()) {
      file.createNewFile()
    }

    val writer = new BufferedWriter(new FileWriter(file))

    writer.write(header.mkString(","))
    writer.newLine()

    for (i <- 0 to (data.length - 1)) {
      writer.write(data(i).mkString(","))
      writer.newLine()
    }

    writer.close()
  }

  /**
    * Writes the result header and content rows to given file path.
    * Result file is created if it does not exist.
    *
    * @param filePath
    * @param headers
    * @param rows
    */
  def writeResultsToFile(filePath: String,
                         headers: Array[String],
                         rows: Array[Vector]): Unit = {

    val file = new File(filePath)
    if (!file.exists()) {
      file.createNewFile()
    }

    val fileWriter = new BufferedWriter(new FileWriter(file))
    fileWriter.write(headers.mkString(","))
    fileWriter.newLine()

    for (row <- rows) {
      val indexedRow = row.toArray.zipWithIndex
      for (col <- indexedRow) {
        if (col._2 > 0) {
          fileWriter.write(",")
        }
        fileWriter.write(col._1.toString)
      }
      fileWriter.newLine()
    }

    fileWriter.close()
  }
}
