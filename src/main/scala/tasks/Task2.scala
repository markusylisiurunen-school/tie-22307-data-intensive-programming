package tasks

import org.apache.log4j.{Logger, Level}
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{SparkSession, DataFrame}

import utils._

object Task2 {

  /**
    * Configuration for the task.
    */
  object Config {
    // Number of clusters
    val k = 50
  }

  /**
    * Calculate the x and y values for the day of the week.
    */
  def buildDayOfWeekConverter(index: Int) = {
    udf((dayOfWeek: String) => {
      val (x, y) = TaskUtils.dayOfWeekToFeatures(dayOfWeek)
      if (index == 0) x else y
    })
  }

  /**
    * Get the KMeansModel for the 3D feature vector.
    */
  def get3DimensionalKMeansModel(spark: SparkSession,
                                 k: Int): (KMeansModel, DataFrame) = {
    // Read the raw data to a DataFrame
    val accidentsDataFrame = spark.read
      .option("header", "true")
      .option("delimiter", ";")
      .option("inferSchema", "true")
      .csv("data/*.csv")

    // Clean the data to only include the needed columns
    accidentsDataFrame.createOrReplaceTempView("accidentsRelation")

    val accidentsQuery =
      """
      SELECT X, Y, Vkpv
      FROM accidentsRelation
      WHERE (X IS NOT NULL AND Y IS NOT NULL AND Vkpv IS NOT NULL)
    """

    val cleanedAccidentsDataFrame = spark.sql(accidentsQuery)

    // TODO: Add new columns for the day of week feature
    val accidentsWithDayOfWeekDataFrame = cleanedAccidentsDataFrame
      .withColumn("Vkpv_x", buildDayOfWeekConverter(0)(col("Vkpv")))
      .withColumn("Vkpv_y", buildDayOfWeekConverter(1)(col("Vkpv")))

    // Extract the features to a feature vector
    val accidentsVectorAssembler = new VectorAssembler()
      .setInputCols(Array("X", "Y", "Vkpv_x", "Vkpv_y"))
      .setOutputCol("features")

    val accidentsWithFeatures = new Pipeline()
      .setStages(Array(accidentsVectorAssembler))
      .fit(accidentsWithDayOfWeekDataFrame)
      .transform(accidentsWithDayOfWeekDataFrame)
      .persist()

    // Build the KMeansModel to be used later
    val kMeansModel = new KMeans()
      .setK(k)
      .setSeed(1L)
      .fit(accidentsWithFeatures)

    return (kMeansModel, accidentsWithFeatures)
  }

  def apply(): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    val spark = SparkSession
      .builder()
      .appName("dip-ostrich-task-2")
      .config("spark.driver.host", "localhost")
      .master("local")
      .getOrCreate()

    spark.conf.set("spark.sql.shuffle.partitions", "5")

    // Create the KMeansModel from the accident data
    val (kMeansModel, accidentsWithFeatures) =
      get3DimensionalKMeansModel(spark, Config.k)

    // TODO: Convert the features to the correct output format
    val outputs = kMeansModel.clusterCenters.toArray
      .map(
        f =>
          Array(
            f(0).toString,
            f(1).toString,
            TaskUtils.dayOfWeekFeaturesToIndex(f(2), f(3)).toString
        ))

    // Write the results to the output file
    FileUtils.write(
      "results/task2.csv",
      Array("x", "y", "dayOfWeek"),
      outputs
    )

    // Release the accident feature dataframe
    accidentsWithFeatures.unpersist()

    spark.stop()
  }
}
