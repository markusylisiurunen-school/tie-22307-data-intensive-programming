package tasks

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.rdd.{RDD}
import org.apache.log4j.{Logger, Level}
import scala.math._

import utils._

object Task6 {

  /**
    * Configuration for the task.
    */
  object Config {
    // Number of clusters to iterate through
    val kMin = 10
    val kMax = 200
    val kIncrement = 10

    // Columns to extract from the initial data and to how many features they map
    val columns = Array(("X", 1), ("Y", 1), ("Vkpv", 2))

    // Define a transform function for each column to get the features
    val transformers = Map(
      "X" -> KMeans.transformToDouble _,
      "Y" -> KMeans.transformToDouble _,
      "Vkpv" -> KMeans.transformToDayOfWeekIndex _
    )

    // Define a output function to get the final output from the features
    val outputs = Map(
      "X" -> KMeans.outputDouble _,
      "Y" -> KMeans.outputDouble _,
      "Vkpv" -> KMeans.outputDayOfWeekIndex _
    )
  }

  // UTILITY FUNCTIONS

  /**
    * Extract the features from the accidents.
    */
  def extractFeatures(header: Array[String],
                      accidents: RDD[String]): RDD[Array[Double]] = {
    var indexes = Array[Int]()

    for (i <- 0 until Config.columns.length) {
      indexes = indexes :+ header.indexOf(Config.columns(i)._1)
    }

    accidents
      .map(_.split(";"))
      .filter(
        columns =>
          columns.deep != header.deep && !indexes
            .map(i => columns(i))
            .contains(""))
      .map(columns =>
        indexes.flatMap(i => Config.transformers(header(i))(columns(i))))
  }

  /**
    * Task entry point.
    */
  def apply(): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    // Create the Spark context
    val sc = new SparkContext(
      new SparkConf()
        .setAppName("dip-ostrich-task-6")
        .setMaster("local")
    )

    // Read the accident file
    val accidentFileName = "data/*.csv"
    val accidentFile = sc.textFile(accidentFileName)

    // Extract the features from the data
    val header = accidentFile.first().split(";")
    val accidents = extractFeatures(header, accidentFile).persist()

    // Store the k values and sum of squared errors
    var errors = Array[(Int, Double)]()

    var k = Config.kMin

    while (k <= Config.kMax) {
      val means = KMeans(accidents, k)
      val error = KMeans
        .clusters(means, accidents)
        .map({
          case (i, features) =>
            means(i).zip(features).map({ case (x, y) => pow(x - y, 2) }).sum
        })
        .reduce(_ + _)

      errors = errors :+ (k, error)

      TaskUtils.log(s"Task6: Finished for k = $k")

      k += Config.kIncrement
    }

    val errorOutputs = errors.map {
      case (a, b) => Array(a.toString, b.toString)
    }

    FileUtils.write("results/task6.csv", Array("k", "error"), errorOutputs)

    accidents.unpersist()
    sc.stop()
  }
}
