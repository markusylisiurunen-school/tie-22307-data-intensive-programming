package tasks

import org.apache.log4j.{Logger, Level}
import org.apache.spark.rdd.{RDD}
import org.apache.spark.{SparkContext, SparkConf}

import utils._

object Task5 {

  /**
    * Configuration for the task.
    */
  object Config {
    // Number of clusters
    val k = 50

    // Columns to extract from the initial data and to how many features they map
    val columns = Array(("X", 1), ("Y", 1), ("Vkpv", 2))

    // Define a transform function for each column to get the features
    val transformers = Map(
      "X" -> KMeans.transformToDouble _,
      "Y" -> KMeans.transformToDouble _,
      "Vkpv" -> KMeans.transformToDayOfWeekIndex _
    )

    // Define an output function to get the final output from the features
    val outputs = Map(
      "X" -> KMeans.outputDouble _,
      "Y" -> KMeans.outputDouble _,
      "Vkpv" -> KMeans.outputDayOfWeekIndex _
    )
  }

  // UTILITY FUNCTIONS

  /**
    * Extract the features from the accidents.
    */
  def extractFeatures(header: Array[String],
                      accidents: RDD[String]): RDD[Array[Double]] = {
    var indexes = Array[Int]()

    for (i <- 0 until Config.columns.length) {
      indexes = indexes :+ header.indexOf(Config.columns(i)._1)
    }

    accidents
      .map(_.split(";"))
      .filter(columns =>
        columns(0) != header(0) && !indexes.map(i => columns(i)).contains(""))
      .map(columns =>
        indexes.flatMap(i => Config.transformers(header(i))(columns(i))))
  }

  /**
    * Task 5 entry point.
    */
  def apply(): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    // Create the Spark context
    val sc = new SparkContext(
      new SparkConf()
        .setAppName("dip-ostrich-task-5")
        .setMaster("local")
    )

    // Read the accident file
    val accidentFileName = "data/*.csv"
    val accidentFile = sc.textFile(accidentFileName)

    // Extract the features from the data
    val header = accidentFile.first().split(";")
    val accidents = extractFeatures(header, accidentFile).persist()

    // Run the K-means algorithm for the accidents
    val means = KMeans(accidents, Config.k)

    // Format the outputs
    val outputs = means.map(mean => {
      var output = Array[String]()
      var start = 0

      Config.columns.foreach {
        case (name, count) => {
          output = output ++ Config.outputs(name)(
            mean.slice(start, start + count))
          start += count
        }
      }

      output
    })

    FileUtils.write("results/task5.csv", Config.columns.map(_._1), outputs)

    accidents.unpersist()
    sc.stop()
  }
}
