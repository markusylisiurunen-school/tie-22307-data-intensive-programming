package tasks

import org.apache.log4j.{Logger, Level}
import org.apache.spark.ml.clustering.KMeansModel
import org.apache.spark.sql.{SparkSession, DataFrame}

import utils._

object Task4 {

  /**
    * Get the KMeansModel to use.
    */
  def getKMeansModel(spark: SparkSession, k: Int, dimension: Int) = {
    dimension match {
      case 2 => Task1.get2DimensionalKMeansModel(spark, k)
      case 3 => Task2.get3DimensionalKMeansModel(spark, k)
      case _ => throw new Exception("Invalid dimension.")
    }
  }

  /**
    * Calculate the error for a given KMeansModel.
    */
  def calculateError(kMeansModel: KMeansModel,
                     accidentsWithFeatures: DataFrame): Double = {
    kMeansModel.computeCost(accidentsWithFeatures)
  }

  def apply(): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    val spark = SparkSession
      .builder()
      .appName("dip-ostrich-task-4")
      .config("spark.driver.host", "localhost")
      .master("local")
      .getOrCreate()

    spark.conf.set("spark.sql.shuffle.partitions", "5")

    // Calculate the costs for both 2D and 3D cases for different k values
    var costs = Map(
      2 -> Array[(Int, Double)](),
      3 -> Array[(Int, Double)]()
    )

    for (k <- 10 to 200 by 10) {
      for (d <- 2 to 3) {
        val (kMeansModel, accidentsWithFeatures) = getKMeansModel(spark, k, d)

        costs = costs + (d -> (costs(d) :+ (k, calculateError(
          kMeansModel,
          accidentsWithFeatures))))

        accidentsWithFeatures.unpersist()
      }

      TaskUtils.log(s"Done calculating the error for k = $k")
    }

    // Write the errors to the result files
    for (d <- 2 to 3) {
      FileUtils.write(
        s"results/task4_${d}d.csv",
        Array("k", "error"),
        costs(d).map { case (k, error) => Array(k.toString, error.toString) }
      )
    }

    spark.stop()
  }
}
