package tasks

import org.apache.log4j.{Logger, Level}
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{SparkSession, DataFrame}

import utils._

object Task1 {

  /**
    * Configuration for the task.
    */
  object Config {
    // Number of clusters
    val k = 50
  }

  /**
    * Get the KMeansModel for the 2D feature vector.
    */
  def get2DimensionalKMeansModel(spark: SparkSession,
                                 k: Int): (KMeansModel, DataFrame) = {
    // Read the raw data to a DataFrame
    val accidentsDataFrame = spark.read
      .option("header", "true")
      .option("delimiter", ";")
      .option("inferSchema", "true")
      .csv("data/*.csv")

    // Clean the data to only include the needed columns
    accidentsDataFrame.createOrReplaceTempView("accidentsRelation")

    val accidentsQuery = """
      SELECT X, Y
      FROM accidentsRelation
      WHERE (X IS NOT NULL AND Y IS NOT NULL)
    """

    val cleanedAccidentsDataFrame = spark.sql(accidentsQuery)

    // Extract the features to a feature vector
    val accidentsVectorAssembler = new VectorAssembler()
      .setInputCols(Array("X", "Y"))
      .setOutputCol("features")

    val accidentsWithFeatures = new Pipeline()
      .setStages(Array(accidentsVectorAssembler))
      .fit(cleanedAccidentsDataFrame)
      .transform(cleanedAccidentsDataFrame)
      .persist()

    // Build the KMeansModel to be used later
    val kMeansModel = new KMeans()
      .setK(k)
      .setSeed(1L)
      .fit(accidentsWithFeatures)

    return (kMeansModel, accidentsWithFeatures)
  }

  def apply(): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)

    val spark = SparkSession
      .builder()
      .appName("dip-ostrich-task-1")
      .config("spark.driver.host", "localhost")
      .master("local")
      .getOrCreate()

    spark.conf.set("spark.sql.shuffle.partitions", "5")

    // Create the KMeansModel from the accident data
    val (kMeansModel, accidentsWithFeatures) =
      get2DimensionalKMeansModel(spark, Config.k)

    // Write the results to the output file
    FileUtils.writeResultsToFile(
      "results/basic.csv",
      Array("x,y"),
      kMeansModel.clusterCenters
    )

    // Release the accident feature dataframe
    accidentsWithFeatures.unpersist()

    spark.stop()
  }
}
