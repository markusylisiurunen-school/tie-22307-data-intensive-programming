import tasks._

object Assignment extends App {
  val taskIndex = args.indexOf("-task")

  if (taskIndex == -1) {
    println("Invalid task.")
    sys.exit(1)
  }

  try {
    val task = args(taskIndex + 1).toInt

    task match {
      case 1 => Task1()
      case 2 => Task2()
      // case 3 => Task3()
      case 4 => Task4()
      case 5 => Task5()
      case 6 => Task6()
      case _ => println("Not implemented!")
    }
  } catch {
    case e: NumberFormatException => {
      println("Invalid task.")
      sys.exit(1)
    }
  }
}
